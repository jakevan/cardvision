from PIL import Image

import genwidth

img = Image.open('hdr.png', 'r')
def make_hdr(pil_img):
    pil_img.paste(img, (genwidth.IMG_WIDTH - 80, genwidth.IMG_HEIGHT - 30))
    return pil_img
