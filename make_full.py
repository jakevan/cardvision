import cv2
import time
import os
import numpy as np
from PIL import Image, ImageDraw, ImageFilter
import json
from pathlib import Path

import genwidth
from pixutils import pix_dist


def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return np.asarray(img)
    return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


# Setup data
Path("data").mkdir(exist_ok=True)
types = ["D6_1", "D6_2", "D6_3", "D6_4", "D6_5", "D6_6"]
current_type = 0  # 13
standby = True

# define a video capture object
vid = cv2.VideoCapture(0)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, genwidth.IMG_WIDTH)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, genwidth.IMG_HEIGHT)
vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
focus = 7
vid.set(cv2.CAP_PROP_EXPOSURE, 100)
tran = 0
start = time.time()


print(">>> Press n to generate: " + str(types[current_type]))
while True:
    tran += 1
    ret, frame = vid.read()

    pil_image = convert_from_cv2_to_image(frame)
    clean_img = convert_from_cv2_to_image(frame)

    imd = ImageDraw.Draw(pil_image)
    # 640 width, 100 card width,. 640/2 = 320+50
    # 480L 140CL =
    center = (640//2, 480//2)
    rad = 25
    imd.point(center)
    imd.ellipse([center[0] - rad, center[1] - rad, center[0] + rad, center[1] + rad], outline='red')
    marked_frame = convert_from_image_to_cv2(pil_image)

    small = cv2.resize(marked_frame, (0, 0), fx=2, fy=2)
    cv2.imshow('frame', small)

    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        break
    elif k == ord('a'):
        focus -= 1
        print(focus)
    elif k == ord('s'):
        focus += 1
        print(focus)
    elif k == ord('n'):
        current_type += 1
        time.sleep(0.5)
        print(">>> Press n to generate: " + str(types[current_type]))
    elif k == ord('g'):
        fname = str(round(time.time() * 1000))
        data_dir = "data/%s/%s.png" % (types[current_type], fname)
        Path(os.path.dirname(data_dir)).mkdir(parents=True, exist_ok=True)
        cv2.imwrite(data_dir, frame)



    vid.set(cv2.CAP_PROP_FOCUS, focus)


# TODO Commit or delete

vid.release()
cv2.destroyAllWindows()

end = time.time()

elapsed = end - start
print("Elapsed: %d, Frames: %d, FPS: %s" % (elapsed, tran, tran / elapsed))
