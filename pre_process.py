import os
import pathlib
import shutil
from PIL import Image, ImageDraw, ImageOps


# The images from unprocessed_data need to be:
# rotated 90 if applicable
# converted to greyscale
# resized to 480p 4:3
# inverted

data_dir = pathlib.Path("unprocessed_data")

files = list(data_dir.glob('*/*/*.jpg'))
for f in files:
    dice_type = f.parent.parent.name
    dice_num = f.parent.name
    filepath = "data/" + dice_type + "_" + dice_num + "/" + f.name
    pathlib.Path(os.path.dirname(filepath)).mkdir(parents=True, exist_ok=True)

    img = Image.open(str(f))
    if img.width < img.height:
        img = img.rotate(90, expand=True)
    img = img.convert("L")
    img = img.resize((640, 480), box=(504, 0, img.width-504, img.height))
    img.save(filepath)

    # img = ImageOps.invert(img)
    # img.save(filepath[:-4] + "inv.jpg")



