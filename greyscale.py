import os
import pathlib
import shutil
from PIL import Image, ImageDraw, ImageOps


data_dir = pathlib.Path("data")

files = list(data_dir.glob('*/*.png'))
for f in files:
    pathlib.Path(os.path.dirname(f)).mkdir(parents=True, exist_ok=True)
    img = Image.open(str(f))
    img = img.convert("L")
    img.save(f)



