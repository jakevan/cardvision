import math
import random
import time
from pathlib import Path

from PIL import Image, ImageDraw, ImageChops, ImageFilter
import cv2 as cv
import numpy as np
from tqdm import tqdm

import genwidth
import pixutils
from pixutils import pix_dist
from matplotlib import pyplot as plt
import os, random


def random_path_from_dir(directory):
    return directory + '/' + random.choice(os.listdir(directory))


def rearrange_pts(pts):
    # [[0,1], [2,3]
    small_y_index = 0
    big_y_index = 0

    small_y_index_2 = 0
    big_y_index_2 = 0
    # Get biggest and smallest y
    for i in range(4):
        if pts[i][1] < pts[small_y_index][1]:
            small_y_index = i
        if pts[i][1] > pts[big_y_index][1]:
            big_y_index = i

    # Get 2nd smallest
    indexes = [0, 1, 2, 3]
    indexes.remove(small_y_index)
    indexes.remove(big_y_index)

    if pts[indexes[0]][1] > pts[indexes[1]][1]:
        big_y_index_2 = indexes[0]
        small_y_index_2 = indexes[1]
    else:
        big_y_index_2 = indexes[1]
        small_y_index_2 = indexes[0]

    # Now, we know the top 2 and bottom 2 points, sort them by L/R
    if pts[big_y_index][0] < pts[big_y_index_2][0]:
        bl = pts[big_y_index]
        br = pts[big_y_index_2]
    else:
        bl = pts[big_y_index_2]
        br = pts[big_y_index]

    if pts[small_y_index][0] < pts[small_y_index_2][0]:
        tl = pts[small_y_index]
        tr = pts[small_y_index_2]
    else:
        tl = pts[small_y_index_2]
        tr = pts[small_y_index]

    return [tl, tr, bl, br]

ty = pixutils.types
ran = 0
for t in ty:
    ran += 1
    print("Running %d of %d" % (ran, len(ty)))
    for to_run in tqdm(range(10)):
        bg = Image.open(random_path_from_dir('datagen/bgs'))  # TODO randomly select
        img = cv.imread(random_path_from_dir('datagen/cards/' + t), cv.IMREAD_UNCHANGED)
        rows, cols, ch = img.shape
        framerect = [18 + 5, 18 + 5, 112, 160]
        pts1 = np.float32([framerect[:2],
                           [framerect[0] + framerect[2], framerect[1]],
                           [framerect[0], framerect[1] + framerect[3]],
                           [framerect[0] + framerect[2], framerect[1] + framerect[3]]])

        MAX_TRANSFORM = 7  # 18  # px # was 10
        trns_x = random.randint(-MAX_TRANSFORM, MAX_TRANSFORM)
        trns_y = random.randint(-MAX_TRANSFORM, MAX_TRANSFORM)
        trns_x2 = random.randint(-MAX_TRANSFORM, MAX_TRANSFORM)
        trns_y2 = random.randint(-MAX_TRANSFORM, MAX_TRANSFORM)
        pts2 = pts1.copy()

        n = random.randint(0, 1)
        # n = 3
        if n == 0:
            # TOP/BOTTOM points
            pts2[0][0] += trns_x
            pts2[0][1] += trns_y
            pts2[1][0] -= trns_x
            pts2[1][1] += trns_y

            pts2[2][0] += trns_x2
            pts2[2][1] += trns_y2
            pts2[3][0] -= trns_x2
            pts2[3][1] += trns_y2
        elif n == 1:
            # L/R points
            pts2[0][0] += trns_x
            pts2[0][1] += trns_y
            pts2[2][0] += trns_x
            pts2[2][1] -= trns_y

            pts2[1][0] += trns_x2
            pts2[1][1] += trns_y2
            pts2[3][0] += trns_x2
            pts2[3][1] -= trns_y2


        def convert_from_cv2_to_image(img: np.ndarray) -> Image:
            return Image.fromarray(cv.cvtColor(img, cv.COLOR_BGR2RGB))


        def convert_from_image_to_cv2(img: Image) -> np.ndarray:
            # return np.asarray(img)
            return cv.cvtColor(np.array(img), cv.COLOR_RGB2BGR)


        M = cv.getPerspectiveTransform(pts1, pts2)
        dst = cv.warpPerspective(img, M, (200, 200))

        card = convert_from_cv2_to_image(dst)
        card = ImageChops.offset(card, 21, 0)  # 200 - 158[original image size] = 42, half the image = 21

        offset = (random.randint(0, genwidth.IMG_WIDTH - 150), random.randint(0, genwidth.IMG_HEIGHT - 150))

        # convert card to rgba
        clean_img = card.convert("RGBA")
        pixdata = clean_img.load()
        width, height = clean_img.size

        for y in range(height):
            for x in range(width):
                dat = pixdata[x, y]
                if dat[0] == dat[1] == dat[2] or pix_dist(dat, (255, 255, 255)) < 500:
                    pixdata[x, y] = (255, 255, 255, 0)

#clean_img = clean_img.filter(ImageFilter.GaussianBlur(radius=1))
        # clean_img.show()

        angle = random.randint(0, 360)
        clean_img = clean_img.rotate(angle, expand=False)
        # Since rotate expands the image, it needs to be subtracted so center lines up

        bg = bg.convert("RGBA")
        bg.paste(clean_img, offset, clean_img)

        imd = ImageDraw.Draw(bg)
        w_pts = []
        for x in range(4):
            # imd.point((pts1[x][0] + 40, pts1[x][1] + 40), fill='cyan')
            w_pts.append((pts2[x][0] + offset[0] + 21, pts2[x][1] + offset[1]))

        origin = (offset[0] + 100, offset[1] + 100)
        for x in range(4):
            w_pts[x] = pixutils.rotate(origin, w_pts[x], math.radians(-angle))
        # imd.point(origin, fill="red")
        # Order w_pts by TL, TR, BL, BR
        w_pts = rearrange_pts(w_pts)

        # imd.line([w_pts[0], w_pts[1]], fill='blue', width=1)
        # imd.text(w_pts[0], "TOP LEFT", fill='red')
        # imd.line([w_pts[0], w_pts[2]], fill='blue', width=1)
        # imd.text(w_pts[1], "TOP RIGHT", fill='red')
        # imd.line([w_pts[3], w_pts[1]], fill='blue', width=1)
        # imd.text(w_pts[2], "BOTTOM LEFT", fill='red')
        # imd.line([w_pts[3], w_pts[2]], fill='blue', width=1)
        # imd.text(w_pts[3], "BOTTOM RIGHT", fill='red')
        # bg.show()

        bg = bg.convert("RGB")
        bg = bg.filter(ImageFilter.GaussianBlur(radius=0.5+random.random()))

        fname = str(round(time.time() * 1000))
        path = "data/%s/_%d_%d_%d_%d_%d_%d_%d_%d_%s.png" % (t,
                                                            w_pts[0][0], w_pts[0][1],
                                                            w_pts[1][0], w_pts[1][1],
                                                            w_pts[2][0], w_pts[2][1],
                                                            w_pts[3][0], w_pts[3][1],
                                                            fname)

        Path(os.path.dirname(path)).mkdir(parents=True, exist_ok=True)
        bg.save(path)
