import cv2
import time
import os
import numpy as np
from PIL import Image, ImageDraw
import json
from pathlib import Path
import genwidth


def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return np.asarray(img)
    return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


# Setup data
Path("datagen/bgs").mkdir(exist_ok=True)

# define a video capture object
vid = cv2.VideoCapture(0)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, genwidth.IMG_WIDTH)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, genwidth.IMG_HEIGHT)
vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
tran = 0
start = time.time()
while True:
    tran += 1
    ret, frame = vid.read()

    small = cv2.resize(frame, (0, 0), fx=2, fy=2)
    cv2.imshow('frame', small)

    if cv2.waitKey(1) & 0xFF == ord('n'):
        fname = str(round(time.time() * 1000))
        data_dir = "datagen/bgs/%s.png" % (fname)
        Path(os.path.dirname(data_dir)).mkdir(parents=True, exist_ok=True)
        # Single Mode
        cv2.imwrite(data_dir, frame)

# TODO Commit or delete

vid.release()
cv2.destroyAllWindows()

end = time.time()

elapsed = end-start
print("Elapsed: %d, Frames: %d, FPS: %s" % (elapsed, tran, tran/elapsed))