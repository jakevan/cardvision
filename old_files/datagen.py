import cv2
import time
import os
import numpy as np
from PIL import Image, ImageDraw
import json
from pathlib import Path

import dataextract
import globalwidth
import hdr


def convert_from_cv2_to_image(img: np.ndarray) -> Image:
    return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


def convert_from_image_to_cv2(img: Image) -> np.ndarray:
    # return np.asarray(img)
    return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


# Setup data
Path("data").mkdir(exist_ok=True)

types = ["ACE", "TWO", "THREE", "FOUR", "FIVE",
         "SIX", "SEVEN", "EIGHT", "NINE", "TEN",
         "JACK", "QUEEN", "KING", "POSITIONAL"]
current_type = 4 # 13
standby = True


# define a video capture object
vid = cv2.VideoCapture(0)
vid.set(cv2.CAP_PROP_FRAME_WIDTH, globalwidth.IMG_WIDTH)
vid.set(cv2.CAP_PROP_FRAME_HEIGHT, globalwidth.IMG_HEIGHT)
vid.set(cv2.CAP_PROP_AUTOFOCUS, 0)
tran = 0
start = time.time()
print(">>> ON STANDBY, PRESS 'n' TO BEGIN GENERATING DATA: " + str(types[current_type]))

while True:
    tran += 1
    ret, frame = vid.read()

    pil_image = convert_from_cv2_to_image(frame)
    coords = dataextract.get_coords(pil_image)
    # cleaned = dataextract.clean(convert_from_cv2_to_image(frame))
    marked_frame = convert_from_image_to_cv2(coords[0])

    small = cv2.resize(marked_frame, (0, 0), fx=4, fy=4)
    cv2.imshow('frame', small)

    if not standby and (coords[1] != 0 or current_type != 13):
        fname = str(round(time.time() * 1000))
        if current_type == 13:
            data_dir = "posdata/%s.jpg_%d_%d" % (fname, coords[1], coords[2])
        else:
            data_dir = "data/%s/%s.jpg" % (types[current_type], fname)
        Path(os.path.dirname(data_dir)).mkdir(parents=True, exist_ok=True)
        # Single Mode
        cv2.imwrite(data_dir, frame)
        # cleaned.save(data_dir)


    if cv2.waitKey(1) & 0xFF == ord('n'):


        # Multi Mode
        if standby:
            standby = False
            print("Get ready to train: " + str(types[current_type]))
            time.sleep(1)
            print("Done sleeping!")
        else:
            standby = True
            print("Finished training!")
            break

# TODO Commit or delete

vid.release()
cv2.destroyAllWindows()

end = time.time()

elapsed = end-start
print("Elapsed: %d, Frames: %d, FPS: %s" % (elapsed, tran, tran/elapsed))