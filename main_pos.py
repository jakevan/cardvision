import glob
import os

import cv2
import matplotlib.pyplot as plt
import numpy as np
import PIL
import tensorflow as tf
from PIL import Image, ImageDraw
from keras import Input

from tensorflow import keras
from tensorflow.keras import datasets, layers, models
from tensorflow.keras.models import Sequential

import pathlib

import genwidth

data_dir = pathlib.Path("datagen")

image_count = len(list(data_dir.glob('*/*.png*')))

print(image_count)

batch_size = 32
img_height = genwidth.IMG_HEIGHT
img_width = genwidth.IMG_WIDTH

# list_ds = tf.data.Dataset.list_files(str(data_dir / '*/*'), shuffle=False)
# list_ds = list_ds.shuffle(image_count, reshuffle_each_iteration=False)
filearray = []
for f in glob.glob("datagen/*/*.png"):
    filearray.append(f)

list_ds = tf.data.Dataset.from_tensor_slices(np.array(filearray))
list_ds.shuffle(image_count, reshuffle_each_iteration=False)

class_names = np.array(sorted(
    [item.name for item in data_dir.glob('*') if item.name != "LICENSE.txt" and not item.name.endswith(".json")]))
print(class_names)

val_size = int(image_count * 0.2)
train_ds = list_ds.skip(val_size)
val_ds = list_ds.take(val_size)

def get_label(file_path):
    parts = tf.strings.split(file_path, os.path.sep)
    one_hot = parts[-2] == class_names
    return tf.argmax(one_hot)

def get_xy(file_path):
    parts = tf.strings.split(file_path, '_')
    # No lists allowed here... cause its a tensor or something. Easier to just return 8 things
    return float(parts[-9]), float(parts[-8]), float(parts[-7]), float(parts[-6]), float(parts[-5]), float(parts[-4]), float(parts[-3]), float(parts[-2])

def decode_img(img):
    img = tf.io.decode_jpeg(img, channels=3)
    return tf.image.resize(img, [img_height, img_width])  # resize probs not needed


def process_path(file_path):
    # label = get_label(file_path)
    img = tf.io.read_file(file_path)
    img = decode_img(img)
    xy = get_xy(file_path)

    return img, tf.stack([xy[0],xy[1],xy[2],xy[3],xy[4],xy[5],xy[6],xy[7]])


AUTOTUNE = tf.data.AUTOTUNE
train_ds = train_ds.map(process_path, num_parallel_calls=AUTOTUNE)
val_ds = val_ds.map(process_path, num_parallel_calls=AUTOTUNE)


def configure_for_performance(ds):
    # ds = ds.cache()
    ds = ds.shuffle(buffer_size=1000)
    ds = ds.batch(batch_size)
    ds = ds.prefetch(buffer_size=AUTOTUNE)
    return ds

train_ds = configure_for_performance(train_ds)
val_ds = configure_for_performance(val_ds)

# plt.figure(figsize=(20, 20))
# for images, lbl in train_ds.take(1):
#   for i in range(19):
#     ax = plt.subplot(5, 5, i + 1)
#     plt.imshow(images[i].numpy().astype("uint8"))
#     plt.title(str(lbl[i].numpy()))
#     plt.axis("off")
# plt.show()

num_classes = len(class_names)
data_augmentation = keras.Sequential(
    [
        layers.RandomBrightness(0.1, input_shape=(img_height, img_width, 3)),
        layers.RandomContrast(0.1),
        layers.RandomZoom(0.02),
    ]
)

model = Sequential([
    data_augmentation,
    layers.Rescaling(1. / 255),
    layers.Conv2D(16, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(32, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Conv2D(64, 3, padding='same', activation='relu'),
    layers.MaxPooling2D(),
    layers.Dropout(0.1),
    layers.Flatten(),
    layers.Dense(256, activation='relu'),
    layers.Dense(128, activation='relu'),
    layers.Dense(64, activation='relu'),
    layers.Dense(8, activation='relu')
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.LogCosh(),
              metrics=['accuracy'])

model.summary()

if os.path.isfile("save/model2.h5"):
    model.load_weights("save/model2.h5")
else:
    epochs = 30  # 10 actually works
    history = model.fit(
        train_ds,
        validation_data=val_ds,
        epochs=epochs
    )
    model.save_weights("save/model2.h5")

    acc = history.history['accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    # plt.show()

def get_pos_model():
    return model

if __name__ == "__main__":
    # exit(1)
    vid = cv2.VideoCapture(0)
    vid.set(cv2.CAP_PROP_FRAME_WIDTH, genwidth.IMG_WIDTH)
    vid.set(cv2.CAP_PROP_FRAME_HEIGHT, genwidth.IMG_HEIGHT)


    def convert_from_cv2_to_image(img: np.ndarray) -> Image:
        return Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))


    def convert_from_image_to_cv2(img: Image) -> np.ndarray:
        return cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)


    while True:
        ret, frame = vid.read()

        # frame = cv2.GaussianBlur(frame, (9, 9), 0)

        img = convert_from_cv2_to_image(frame)
        imd = ImageDraw.Draw(img)

        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        rgb_tensor = tf.convert_to_tensor(rgb, dtype=tf.float32)

        img_array = tf.expand_dims(rgb_tensor, 0)

        predictions = model.predict(img_array, steps=1)

        p0 = predictions[0]
        w_pts = []
        for i in range(0, 8, 2):
            w_pts.append((p0[i], p0[i+1]))

        imd.line([w_pts[0], w_pts[1]], fill='blue', width=1)
        imd.line([w_pts[0], w_pts[2]], fill='blue', width=1)
        imd.line([w_pts[3], w_pts[1]], fill='blue', width=1)
        imd.line([w_pts[3], w_pts[2]], fill='blue', width=1)

        cv2img = convert_from_image_to_cv2(img)
        cv2img = cv2.resize(cv2img, (0, 0), fx=2, fy=2)
        cv2.imshow('frame', cv2img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    vid.release()
    cv2.destroyAllWindows()
